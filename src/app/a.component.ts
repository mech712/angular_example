import { Component, Input } from '@angular/core';
import { CountService } from './count.service'

 
@Component({
    selector: 'acomp',
    template: `<p>nocommon:{{_nocommon}} {{count}}</p> <button (click)="decrease()">-</button><button (click)="increase()">+</button>`
})
export class AComponent { 
    private _nocommon:boolean = false;
    constructor(private countService:CountService){}

    increase(){this.countService.increment();}
    decrease(){this.countService.decrement();}

    @Input() set nocommon(b:boolean){ 
        this._nocommon=b;
        if (this._nocommon) this.countService = new CountService();
    }
    
    get count():number {return this.countService.count;}

}