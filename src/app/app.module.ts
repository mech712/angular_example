import {NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }   from './app.component';
import { AComponent } from './a.component';
import { CountService } from './count.service'

@NgModule({
    imports:      [ BrowserModule, FormsModule ],
    declarations: [ AppComponent, AComponent ],
    bootstrap:    [ AppComponent ],
    providers:[CountService]
})
export class AppModule { }